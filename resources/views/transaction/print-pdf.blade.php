<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pdf</title>
</head>
<body>
    <h4>{{ $data->created_at }}</h4>
    
    <table border="1" cellpadding="10" cellspacing="0" width="100%">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama Baranag</th>
                <th scope="col">Harga Satuan</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Jumlah Harga</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total_harga = 0;
            @endphp
            @forelse ($data->TransaksiPembelianBarang as $index => $item)
            @php $total = $item->harga_satuan * $item->jumlah @endphp
            <tr>
                <th scope="row">{{ $index + 1 }}</th>
                <td>{{ $item->MasterBarang->nama_barang }}</td>
                <td>{{ $item->harga_satuan }}</td>
                <td>{{ $item->jumlah }}</td>
                <td>{{ $total }}</td>
                </tr>
            @php
                $total_harga += $total;
            @endphp
            @empty
                <tr>
                    <td class="text-center" colspan="6"> <p>Belum Ada Data</p> </td>
                </tr>
            @endforelse
        </tbody>
    </table>

    <h4>Total Harga Rp. {{ $total_harga }}</h4>
</body>
</html>