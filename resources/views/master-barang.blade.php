@extends('layouts.master')

@section('name')
    Master Barang
@endsection

@section('content')
    
<div class="container-fluid">

    <div class="card position-relative">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Detail Transaksi</h6>
        </div>
    <div class="card-body">
        <table class="table form">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Harga</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($barang as $index => $item)
                <tr>
                  <th scope="row"> {{ $index + 1 }} </th>
                  <td> {{ $item->nama_barang }} </td>
                  <td> {{ $item->harga_satuan }}</td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>
                    

@endsection