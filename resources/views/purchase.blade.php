@extends('layouts.master')

@section('name')
    Transaksi Pembelian Barang
@endsection

@section('content')

<form action="{{ url('/purchase/' . $transaksi_pembelian_id) }}" class="purchase">
    <input type="hidden" name="namabarang" class="purchase-namabarang">
</form>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Content Row -->
    <div class="row">

        <!-- Grow In Utility -->
        <div class="col-lg-8">
            <div class="card position-relative">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Table Transaksi</h6>
                </div>
                <div class="card-body">
                    <form action="{{ url('/purchase/' . $transaksi_pembelian_id) }}" method="post">
                        @csrf
                        <div class="form-row">
                                <div class="col">
                                    <label for="exampleFormControlSelect1">Pilih Barang</label>
                                    <select class="form-control select2 nama-barang" name="namabarang" id="exampleFormControlSelect1">
                                    @foreach ($barang as $id => $namabarang)
                                        <option value="{{ $id }}" {{ $id_barang == $id ? 'selected' : null }}>{{ $namabarang }}</option>  
                                    @endforeach
                                    </select>
                                </div>
                            
                            <div class="col">
                                <label for="exampleFormControlSelect1">Harga Satuan</label>
                                <input type="number" class="form-control harga-satuan" name="hargasatuan" placeholder="Harga Satuan" readonly value="{{ $selected_barang != null? (int) $selected_barang->harga_satuan : null }}">
                            </div>
                        </div>
                        <div class="form-row mt-3">
                            <div class="col">
                                <label for="exampleFormControlSelect1">Jumlah</label>
                                <input type="number" class="form-control jumlah-barang" name="jumlahbarang" placeholder="Jumlah Barang" min="1">
                            </div>
                            <div class="col">
                                <label for="exampleFormControlSelect1">Total</label>
                                <input type="number" class="form-control total-harga" name="totalharga" placeholder="Harga Total Barang" readonly>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-3">Tambahkan</button>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-sm">
                        <thead>
                          <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Harga Satuan</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col">Jumlah Harga</th>
                            @if (auth()->user()->role->code == 'ADM')
                                <th scope="col">Action</th>
                            @endif
                          </tr>
                        </thead>
                        <tbody>
                            @php
                                $total_harga = 0;
                            @endphp
                            @forelse ($list_barang as $index => $item)
                            @php $total = $item->harga_satuan * $item->jumlah @endphp
                            <tr>
                                <th scope="row">{{ $index + 1 }}</th>
                                <td>{{ $item->MasterBarang->nama_barang }}</td>
                                <td>{{ $item->harga_satuan }}</td>
                                <td>{{ $item->jumlah }}</td>
                                <td>{{ $total }}</td>
                                @if (auth()->user()->role->code == 'ADM')
                                    <td>
                                        <form action="{{ url('/purchase/' . $transaksi_pembelian_id . '/' . $item->id ) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                @endif
                              </tr>
                            @php
                                $total_harga += $total;
                            @endphp
                            @empty
                              <tr>
                                  <td class="text-center" colspan="6"> <p>Belum Ada Data</p> </td>
                              </tr>
                            @endforelse
                        </tbody>
                      </table>
                </div>
            </div>
        </div>

            <!-- Fade In Utility -->
            <div class="col-lg-4">
                <div class="card position-relative">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Informasi Total - {{ $transaksi->created_at }}</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h4>Total</h4>
                            </div>
                            <div class="col">
                                <h4>Rp. {{ $total_harga }}</h4>
                            </div>
                        </div>
                        <a href="{{ url('transaction') }}" class="btn btn-primary mt-3">Simpan</a>
                        <a href="{{ url('/purchase/' . $transaksi_pembelian_id . '/pdf') }}" class="btn btn-primary mt-3">Print</a>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

</body>

</html>

@endsection

@push('custom-script')
<script>
    $('.nama-barang').on('change', function() {
        let namaBarang = $(this).val();

        $('.purchase-namabarang').val(namaBarang);
        $('.purchase').submit();
    });
 
    $('.jumlah-barang').on('change', function() {
        let jumlah = $(this).val();
        let hargaSatuan = $('.harga-satuan').val();
        let totalHarga = hargaSatuan * jumlah;

        $('.total-harga').val(totalHarga);
    });
</script>
@endpush